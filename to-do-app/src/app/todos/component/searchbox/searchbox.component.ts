import { Component, OnInit } from "@angular/core";
import { async, Observable } from 'rxjs';
import { FilterEnum, TodoService } from "../../services/todo.services";
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'search-box',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})

export class SearchBoxComponent implements OnInit {
  isOpen = false;
  searchValue = "";
  filter$: Observable<FilterEnum>;
  filterEnum = FilterEnum;

  constructor(private todoService: TodoService, private storageService: StorageService) {
    this.filter$ = this.todoService.filter$
  }

  handleSearch() {
    if (this.isOpen == false) {
      this.isOpen = true;
    } else {
      this.todoService.todo$.next(this.storageService.searchTodos(this.searchValue.trim()))
    }
  }

  handleRefresh() {
    this.isOpen = false;
    this.todoService.todo$.next(this.storageService.searchTodos(""))
  }

  ngOnInit(): void {

  }
}

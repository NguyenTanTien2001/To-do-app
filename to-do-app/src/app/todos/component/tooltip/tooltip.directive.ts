import { Directive, ElementRef, HostListener, Input, OnDestroy } from "@angular/core";
import { Placement } from '../../model/tooltip.placement.model'

@Directive({
  selector: '[tooltip]',
})

export class TooltipDirective implements OnDestroy {
  @Input() tooltip = "";
  @Input() tooltipDelay?= 150;
  @Input() tooltipColor = '#b83f45';
  @Input() tooltipPlace: Placement = 'bottom'

  private myPopup;
  private timer;

  constructor(private elmRef: ElementRef) { }

  ngOnDestroy() {
    if (this.myPopup) {
      this.myPopup.remove()
    }
  }

  getPositionByPlace() {
    let x, y = 0;
    if (this.tooltipPlace === "bottom") {
      x = this.elmRef.nativeElement.getBoundingClientRect().left + this.elmRef.nativeElement.offsetWidth / 2;
      y = this.elmRef.nativeElement.getBoundingClientRect().top + this.elmRef.nativeElement.offsetHeight + 6;
      return { x, y }
    }
    if (this.tooltipPlace === "top") {
      x = this.elmRef.nativeElement.getBoundingClientRect().left + this.elmRef.nativeElement.offsetWidth / 2;
      y = this.elmRef.nativeElement.getBoundingClientRect().top - 45;
      return { x, y }
    }
    return null
  }

  @HostListener('mouseenter') onMouseEnter() {
    const position = this.getPositionByPlace();
    this.timer = setTimeout(() => {
      if (position) {
        this.createTooltipPopup(position.x, position.y);
      }

    }, this.tooltipDelay)
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.timer) clearTimeout(this.timer);
    if (this.myPopup) { this.myPopup.remove() }
  }

  private createTooltipPopup(x: number, y: number) {
    let popup = document.createElement('div');
    popup.innerHTML = this.tooltip;
    popup.setAttribute("class", "tooltip-container");
    popup.style.top = y.toString() + "px";
    popup.style.left = x.toString() + "px";
    popup.style.backgroundColor = this.tooltipColor
    var root = document.querySelector('app-root')
    root.appendChild(popup)
    this.myPopup = popup;
    setTimeout(() => {
      if (this.myPopup) this.myPopup.remove();
    }, 2000); // Remove tooltip after 2 seconds
  }
}

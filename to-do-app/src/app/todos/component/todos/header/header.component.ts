import { Component, OnInit } from '@angular/core';
import { TodoInterface } from 'src/app/todos/model/todo.model';
import { StorageService } from 'src/app/todos/services/storage.service';
import { TodoService } from 'src/app/todos/services/todo.services';

@Component({
  selector: 'app-todos-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  text: string = '';
  error: string = '';

  constructor(private todoService: TodoService, private storageService: StorageService) {
    this.todoService.todo$.subscribe((todo) => {console.log(todo)});
   }

  ngOnInit(): void {
  }

  changeText(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.text = target.value;
  }

  addToDo(): void {
    if(this.text.trim() !== "") {
      const newTodo: TodoInterface = {
        title: this.text,
        description: "",
        tag: [],
        isCompleted: false,
        createDate: new Date(Date.now()).toString(),
        id: Math.random().toString(16)
      }
      this.storageService.addTodoList(newTodo.id);
      this.storageService.addTodo(newTodo);
      this.todoService.addTodo(this.storageService.getTodo(newTodo.id));
      this.text = '';
      this.error = "";
    }
    else {
      this.error = "please enter name of todo"
    }
  }

}

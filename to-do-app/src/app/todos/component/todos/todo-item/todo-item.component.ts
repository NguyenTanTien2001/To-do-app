import {
  Component, ElementRef, EventEmitter, HostListener, Input,
  OnChanges, OnInit, Output, SimpleChanges, ViewChild
} from '@angular/core';
import { TodoInterface } from 'src/app/todos/model/todo.model';
import { StorageService } from 'src/app/todos/services/storage.service';
import { TodoService } from 'src/app/todos/services/todo.services';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
})
export class TodoItemComponent implements OnInit, OnChanges {
  @Input('todo') todoProp: TodoInterface;
  @Input('isEditing') isEditingProp: boolean;
  @Output('setEditingId') setEditingIdEvent: EventEmitter<string | null> = new EventEmitter();
  @Output('setFocusId') setFocusIdEvent: EventEmitter<string | null> = new EventEmitter();

  editingText: string = '';
  error: string = '';

  @ViewChild('textInput') textInput: ElementRef;

  @HostListener("document:click", ['$event'])
  clickedOut(event) {
    if (!this.elmRef.nativeElement.contains(event.target)) {
      if (this.isEditingProp) {
        this.isEditingProp = false;
        this.setEditingIdEvent.emit(null);
      }
    }
  }

  constructor(private todoService: TodoService, private storageService: StorageService, private elmRef: ElementRef) { }

  ngOnInit(): void {
    this.editingText = this.todoProp.title;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('change', changes);
    if (changes !== undefined)
      if (changes['isEditingProp'].currentValue) {
        setTimeout(() => {
          this.textInput.nativeElement.focus();
        }, 0);
      }
  }

  setTodoInEditMode(): void {
    console.log('set todo in edit mode')
    this.setEditingIdEvent.emit(this.todoProp.id)
  }

  removeTodo(): void {
    console.log('remove todo');
    this.storageService.deleteTodoFromList(this.todoProp.id)
    this.storageService.deleteTodo(this.todoProp.id);
    this.todoService.removeTodo(this.todoProp.id);
    if (this.todoService.todo$.getValue().length !== 0) {
      if (this.todoService.focusTodo$.getValue() == this.todoProp.id) {
        this.todoService.focusTodo$.next(this.todoService.todo$.getValue()[0].id);
      }
    }
    else {
      this.todoService.focusTodo$.next("");
    }
  }

  toggleTodo(): void {
    this.storageService.changeTodo(this.todoProp.id, { ...this.todoProp, isCompleted: !this.todoProp.isCompleted })
    this.todoService.toggleTodo(this.todoProp.id, this.storageService.getTodo(this.todoProp.id));
    console.log('toggle todo ', this.todoProp.title, " to ", this.todoProp.isCompleted);

  }

  changeText(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.editingText = target.value;
  }

  changeTodo(): void {
    if (this.editingText.trim() !== "") {
      console.log('change toto to:', this.editingText);
      this.storageService.changeTodo(this.todoProp.id, { ...this.todoProp, title: this.editingText });
      this.todoService.changeTodo(this.todoProp.id, this.storageService.getTodo(this.todoProp.id));
      this.setEditingIdEvent.emit(null);
    } else {
      this.error = "todo name can't be blank"
    }
  }

  setTodoInFocusMode(): void {
    console.log('set todo in focus mode')
    this.setFocusIdEvent.emit(this.todoProp.id);
  }

}

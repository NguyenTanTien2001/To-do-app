import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { TodoInterface } from 'src/app/todos/model/todo.model';
import { StorageService } from 'src/app/todos/services/storage.service';
import { TodoService, FilterEnum } from 'src/app/todos/services/todo.services';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodosListComponent implements OnInit {
  visibleTodo$: Observable<TodoInterface[]>;
  focusTodo$: Observable<TodoInterface>;
  noTodoClass$: Observable<boolean>;
  isAllTodoSelected$: Observable<boolean>;
  editingId: string | null = null;
  isShowInfo = false;

  constructor(private todoService: TodoService, private storageService: StorageService) {
    this.isAllTodoSelected$ = this.todoService.todo$.pipe(map((todos) => todos.every(todo => todo.isCompleted)));
    this.noTodoClass$ = this.todoService.todo$.pipe(map((todos) => todos.length === 0));
    this.visibleTodo$ = combineLatest([this.todoService.todo$, this.todoService.filter$]).pipe(map(([todos, filter]: [TodoInterface[], FilterEnum]) => {
      if(filter === FilterEnum.active) {
        return todos.filter((todo) => !todo.isCompleted)
      } else if (filter === FilterEnum.completed) {
        return todos.filter((todo) => todo.isCompleted)
      }
      return todos;
    }))
    this.focusTodo$ = combineLatest([this.todoService.todo$, this.todoService.focusTodo$]).pipe(map(([todos, focusTodo]: [TodoInterface[], string]) => {
      if(focusTodo !== "") {
        return todos.filter((todo) => todo.id === focusTodo)[0];
      }
      return todos[0];
    }))
   }

  ngOnInit(): void {
  }

  toggleAllTodos(event: Event): void {
    const target = event.target as HTMLInputElement;
    const checked = target.checked;
    const updateTodos = this.todoService.todo$.getValue().map(todo => {
      return {
        ...todo,
        isCompleted: checked,
      };
    })
    this.storageService.addtodos(updateTodos);
    this.todoService.toggleAll(this.storageService.getAllTodo(this.storageService.getTodosList()));
  }

  setEditingId(editingId: string | null): void{
    this.editingId = editingId;
  }

  setFocusId(focusId: string | null): void{
    if (focusId !== this.todoService.focusTodo$.getValue()) {
      this.todoService.focusTodo$.next(focusId);
      console.log("focus id: " + focusId);
      this.isShowInfo = true;
    }
    else {
      this.isShowInfo = !this.isShowInfo;
    }
  }

}

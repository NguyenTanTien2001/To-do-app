import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { BehaviorSubject, combineLatest, map, Observable } from 'rxjs';
import { TodoInterface } from 'src/app/todos/model/todo.model';
import { StorageService } from 'src/app/todos/services/storage.service';
import { TodoService } from 'src/app/todos/services/todo.services';
import { TodosListComponent } from '../todo-list/todo-list.component';

@Component({
  selector: 'app-todo-info',
  templateUrl: './todo-info.component.html',
})
export class TodoInfoComponent implements OnInit {
  @Input('todo') todoProp: TodoInterface;
  @Input('isShowing') isShowingProp: boolean;

  isEditing: boolean = false;
  editingText: string = '';
  title: String = '';

  @ViewChild('textInput') textInput:ElementRef;

  constructor(private todoService: TodoService, private storageService: StorageService) {
  }


  ngOnInit(): void {
    this.editingText = this.todoProp.description;
    this.title = this.todoProp.title;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const currentTodo = JSON.parse(JSON.stringify(changes)).todoProp.currentValue
    console.log('info change', currentTodo);
    this.title = currentTodo.title;
    if(this.isEditing) {
      setTimeout(() => {
        this.textInput.nativeElement.focus();
      }, 0);
    }
  }

  setTodoInEditMode(): void {
    console.log('set todo in edit mode')
    this.isEditing = true;
  }

  changeText(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.editingText = target.value;
  }

  changeTodo(): void {
    console.log('change toto to:', this.editingText);
    this.storageService.changeTodo(this.todoProp.id, {...this.todoProp, description:this.editingText});
    this.todoService.changeTodo(this.todoProp.id, this.storageService.getTodo(this.todoProp.id));
    this.isEditing = false;
  }

}

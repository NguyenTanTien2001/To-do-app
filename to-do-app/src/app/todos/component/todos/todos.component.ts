import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { TodoService } from '../../services/todo.services';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(private todoService: TodoService, private storageService: StorageService) { }

  ngOnInit(): void {
    let todos = this.storageService.getTodosList();
    this.todoService.todo$.next(this.storageService.getAllTodo(todos));
  }

}

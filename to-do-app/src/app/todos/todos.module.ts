import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodosComponent } from 'src/app/todos/component/todos/todos.component';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from 'src/app/todos/component/todos/header/header.component';
import { TodoService } from './services/todo.services';
import { TodosListComponent } from './component/todos/todo-list/todo-list.component';
import { TodoItemComponent } from './component/todos/todo-item/todo-item.component';
import { FooterComponent } from './component/todos/footer/footer.component';
import { SearchBoxComponent } from './component/searchbox/searchbox.component';
import { FormsModule } from '@angular/forms';
import { StorageService } from './services/storage.service';
import { TooltipDirective } from './component/tooltip/tooltip.directive';
import { TodoInfoComponent } from './component/todos/todo-info/todo-info.component';

const routes: Routes = [
  {
    path: '',
    component: TodosComponent,
  }
]

@NgModule({
  declarations: [
    TodosComponent,
    HeaderComponent,
    TodosListComponent,
    TodoItemComponent,
    FooterComponent,
    SearchBoxComponent,
    TooltipDirective,
    TodoInfoComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ],
  providers: [TodoService, StorageService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TodosModule { }

import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { TodoInterface } from "src/app/todos/model/todo.model";
import { StorageService } from "./storage.service";

export enum FilterEnum {
  all = 'all',
  active = 'active',
  completed = 'completed',
}

Injectable()
export class TodoService{
  todo$ = new BehaviorSubject<TodoInterface[]>([]);
  filter$ = new BehaviorSubject<FilterEnum>(FilterEnum.all);
  focusTodo$ = new BehaviorSubject<string>("");

  addTodo(newTodo: TodoInterface): void {

    const updateTodo = [...this.todo$.getValue(), newTodo]
    this.todo$.next(updateTodo)
  }

  toggleAll(changedTodos: Array<TodoInterface>): void {
    this.todo$.next(changedTodos);
  }

  changeFilter(filter: FilterEnum): void {
    this.filter$.next(filter);
  }

  changeTodo(id: string, changedTodo: TodoInterface): void {
    const updateTodos = this.todo$.getValue().map(todo => {
      if(todo.id === id) {
        return changedTodo;
      }
      return todo;
    })
    this.todo$.next(updateTodos);
  }

  removeTodo(id: string): void {
    const updateTodo = this.todo$.getValue().filter(todo => todo.id !== id);
    this.todo$.next(updateTodo);
  }

  toggleTodo(id: string, changedTodo: TodoInterface): void {
    const updateTodos = this.todo$.getValue().map(todo => {
      if(todo.id === id) {
        return changedTodo;
      }
      return todo;
    })
    this.todo$.next(updateTodos);
  }
}

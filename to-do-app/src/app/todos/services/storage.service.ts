import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { TodoInterface } from "src/app/todos/model/todo.model";

export class StorageService {
  saveTodos(todos: Array<string>): void {
    const todos_Json = JSON.stringify(todos);
    //console.log(todos_Json);
    localStorage.setItem('todos', todos_Json);
  }

  addTodoList(todoId: string): void {
    let todosIdArray = this.getTodosList();
    todosIdArray.push(todoId);
    this.saveTodos(todosIdArray);
  }

  addTodo(newTodo: TodoInterface): void {
    let todoid = newTodo.id;
    const newTodo_Json = JSON.stringify(newTodo);
    //console.log(newTodo_Json);
    localStorage.setItem(todoid, newTodo_Json);
  }

  addtodos(newTodos: Array<TodoInterface>): void {
    newTodos.forEach((todo) => {
      this.changeTodo(todo.id, todo);
    })
  }

  getTodosList(): Array<string> {
    if(JSON.parse(localStorage.getItem('todos'))) {
      return JSON.parse(localStorage.getItem('todos'));
    } else {
      return [];
    }
  }

  getTodo(TodoId: string): TodoInterface {
    if (JSON.parse(localStorage.getItem(TodoId))) {
      return JSON.parse(localStorage.getItem(TodoId));
    } else {
      return null;
    }
  }

  getAllTodo(todos: Array<string>): Array<TodoInterface> {
    let todosArray = [];
    todos.forEach((todoId) => {
      let todo = this.getTodo(todoId);
      if (todo != null) todosArray.push(todo);
    })
    return todosArray;
  }

  deleteTodoFromList(deletedTodoId: string): void {
    let todoIdArray = this.getTodosList();
    todoIdArray = todoIdArray.filter((todo) => todo != deletedTodoId);
    this.saveTodos(todoIdArray);
  }

  deleteTodo(deletedTodoId: string): void {
    localStorage.removeItem(deletedTodoId);
  }

  changeTodo(todoid: string, changedTodo: TodoInterface): void {
    if (localStorage.getItem(todoid)) {
      localStorage.setItem(todoid, JSON.stringify(changedTodo));
    } else {
      localStorage.setItem(todoid, JSON.stringify(changedTodo));
    }
  }

  searchTodos(text: string): TodoInterface[] {
    const listTodoId = this.getTodosList();
    let todosArray = [];
    listTodoId.forEach((todoId) => {
      let todo = this.getTodo(todoId);
      if (todo != null && todo.title.includes(text))
        todosArray.push(todo)
    })
    console.log(todosArray)
    return todosArray;
  }
}
